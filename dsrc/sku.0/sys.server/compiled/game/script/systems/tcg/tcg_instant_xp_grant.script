/********************************************************************************************
  Title:       systems.tcg.tcg_instant_xp_grant.script
  Description: Grants the user a percentage of the user's current level.  The percentage is 
 		based off of an objvar on the item itself.

 *******************************************************************************************/

include library.collection; 
include library.prose;
include library.utils;
include library.xp;

const string TBL_PLAYER_LEVEL_XP			= "datatables/player/player_level.iff";
const string_id SID_REWARD_XP				= new string_id("collection","reward_xp_amount");

trigger OnObjectMenuRequest(obj_id player, menu_info mi)
{
	if(utils.isNestedWithinAPlayer(self))
	{
		menu_info_data mid = mi.getMenuItemByType(menu_info_types.ITEM_USE);
		if ( mid != null )
			mid.setServerNotify(true);
		else
			mi.addRootMenu(menu_info_types.ITEM_USE, new string_id("ui_radial", "item_use"));
	}

	return SCRIPT_CONTINUE;
}

trigger OnObjectMenuSelect(obj_id player, int item)
{
	if(utils.getContainingPlayer(self) != player )
		return SCRIPT_CONTINUE;

	// Is the player level 90? If so, grant Collection Item instead.
	if(item == menu_info_types.ITEM_USE)
	{
		if(hasObjVar(self, "grant_xp_percent"))
		{
			// Get the player's level
			int playerLevel = getLevel(player);
			if(playerLevel >= 90)
			{
				// They have no use for XP. Give them a collection item.
				CustomerServiceLog("tcg", "TCG Item("+ self +")used by player: ("+ player +")"
							+ getFirstName(player) + " Player Level: "+ playerLevel +
							". Since player is 90th Lvl, this player receives a random collection object.");
				
				obj_id collectionItem = collection.grantRandomCollectionItem(player, "datatables/loot/loot_items/collectible/magseal_loot.iff", "collections");
				if(!isValidId(collectionItem) || !exists(collectionItem))
				{
					CustomerServiceLog("tcg", "TCG Item ("+ self +") used by player: ("+ player +")"
							+ getFirstName(player) + " Player Level: "+ playerLevel +
							". The collection system reports that a collectible object WAS NOT recieved by the player due to an internal error. Please notify SWG design.");
					return SCRIPT_CONTINUE;
				}

				CustomerServiceLog("buff", "TCG Item ("+ self +") used by player: ("+ player +")"
							+ getFirstName(player) + " Player Level: "+ playerLevel +
							". Collection Item("+ collectionItem +") was received by the player.");
				decrementCount(self);	
				return SCRIPT_CONTINUE;
			}
			else
			{
				float xpPercent = getFloatObjVar(self, "grant_xp_percent");
				// Calculate and find XP amount.
				int xpForCurrentLevel = dataTableGetInt(TBL_PLAYER_LEVEL_XP, playerLevel - 1, "xp_required");
				int xpForNextLevel = dataTableGetInt(TBL_PLAYER_LEVEL_XP, playerLevel, "xp_required");
				// This is the XP needed for player's current level.
				int xpForLevel = xpForNextLevel - xpForCurrentLevel;
				// Make sure xpPercent is made into a percent...
				int xpToGrant = (int)(xpPercent * xpForLevel);

				// Let's make sure XP is greater than nothing
				if(xpToGrant > 0)
				{
					playClientEffectObj(player, "clienteffect/tcg_t16_skyhopper_toy_flyby.cef", player, "root");
					// Grant them the xp
					prose_package pp = new prose_package();
					prose.setStringId(pp, SID_REWARD_XP);
					prose.setDI(pp, xpToGrant);
					sendSystemMessageProse(player, pp);
					// LOG("collectionXp", "ProsePackage Grant: " + (int)xpToGrant);
					xp.grantXpByTemplate(player, xpToGrant);
					CustomerServiceLog("tcg", "Player " + getFirstName(player) + "(" + player + 
								") was granted XP: "+ xpToGrant +" XP from TCG Item: ("
								+ self +")");
					// decrementCount
					decrementCount(self);	
				}
				else
				{
					CustomerServiceLog("tcg", "Player " + getFirstName(player) + "(" + player + 
								") attempted to receive XP from TCG Item: ("+ self +
								") but failed because XP amount was less than Zero.");
				}
			}
		}
		else
		{
			// CS log and end of trigger.
			CustomerServiceLog("tcg", "Player " + getFirstName(player) + "(" + player + 
						") attempted to use TCG Item("+ self +") but can not - item is missing 'grant_xp_percent' objvar)");			
			return SCRIPT_CONTINUE;
		}	
	}

	return SCRIPT_CONTINUE;
}

